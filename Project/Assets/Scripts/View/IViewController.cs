﻿

using Entitas;
using UnityEngine;

/// <summary>
/// Controls View Objects directly, i.e. position, scale, rotation etc.
/// </summary>
public interface IViewController  {

	Vector2 Position { get; set; }
    Vector2 Scale { get; set; }
    bool Active { get; set; }
    void InitializeView(Contexts contexts, IEntity entity);
    void DestroyView();

}

﻿using Entitas;
using UnityEngine;

/// <summary>
/// Listener reacts, when the position of a certain object changes
/// </summary>
public class PositionListener : MonoBehaviour, IEventListener, IPositionListener
{
    private GameEntity _entity;
    public void RegisterListeners(IEntity entity)
    {
        _entity = (GameEntity)entity;
        _entity.AddPositionListener(this);
    }

    public void OnPosition(GameEntity entity, Vector2 newPosition)
    {
        transform.position = newPosition;
        //TODO: handle position change
    }
}
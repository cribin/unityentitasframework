﻿using Entitas;
using UnityEngine;

/// <summary>
/// Implements how the View is represented by Unity
/// </summary>
public class UnityGameView : MonoBehaviour, IViewController
{
    protected Contexts _contexts;
    protected GameEntity _entity;

    public Vector2 Position
    {
        get { return transform.position; }
        set { transform.position = value; }
    }

    public Vector2 Scale
    {
        get { return transform.localScale; }
        set { transform.localScale = value; }
    }

    public bool Active
    {
        get { return gameObject.activeSelf; }
        set { gameObject.SetActive(value); }
    }

    public void InitializeView(Contexts contexts, IEntity entity)
    {
        _contexts = contexts;
        _entity = (GameEntity) entity;
    }

    public void DestroyView()
    {
        Object.Destroy(this);
    }
}
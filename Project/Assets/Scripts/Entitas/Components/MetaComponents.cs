﻿
using Entitas;
using Entitas.CodeGeneration.Attributes;
using UnityEngine;

[Meta, Unique]
public class InputServiceComponent : IComponent
{
    public IInputService Instance;
}

[Meta, Unique]
public class TimeServiceComponent : IComponent
{
    public ITimeService Instance;
    
}

[Meta, Unique]
public class ViewServiceComponent : IComponent
{
    public IViewService Instance;
}
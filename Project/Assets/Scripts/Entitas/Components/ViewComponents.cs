﻿

using Entitas;

/// <summary>
/// The following components holds an instacne to an IViewController(which is required to draw entities to the scene)
/// </summary>
[Game]
public sealed class ViewComponent : IComponent
{
    public IViewController instance;
}

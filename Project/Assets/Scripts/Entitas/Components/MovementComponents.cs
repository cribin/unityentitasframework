﻿using Entitas;
using Entitas.CodeGeneration.Attributes;
using UnityEngine;

[Game, Event(true)]
public class PositionComponent : IComponent
{
    public Vector2 Value;
}

[Game]
public class DirectionComponent : IComponent
{
    public float Value;
}
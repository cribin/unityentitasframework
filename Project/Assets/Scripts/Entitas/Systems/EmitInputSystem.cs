﻿using Entitas;

public class EmitInputSystem : IInitializeSystem, IExecuteSystem
{
    private readonly InputContext _inputContext;
    private readonly IInputService _inputService;
    private InputEntity _inputEntity;

    public EmitInputSystem(Contexts contexts, IInputService inputService)
    {
        _inputContext = contexts.input;
        _inputService = inputService;
    }

    public void Initialize()
    {
        _inputContext.isInput = true;
        _inputEntity = _inputContext.inputEntity;
    }

    public void Execute()
    {
        _inputEntity.ReplaceHorizontalMovement(_inputService.HorizontalInput);
        _inputEntity.isJumpButtonPressed = _inputService.JumpButtonWasPressed;
    }
}
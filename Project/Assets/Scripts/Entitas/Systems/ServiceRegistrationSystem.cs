﻿/// <summary>
/// First feature to be called from the GameController. This class is responsible for injecting necessary dependencies 
/// into all Initialize systems
/// </summary>
public class ServiceRegistrationSystem : Feature
{
    public ServiceRegistrationSystem(Contexts contexts, ServiceContainer serviceContainer)
    {
        Add(new RegisterInputServiceSystem(contexts, serviceContainer.InputService));
        Add(new RegisterTimeServiceSystem(contexts, serviceContainer.TimeService));
        Add(new RegisterViewServiceSystem(contexts, serviceContainer.ViewService));
    }
}
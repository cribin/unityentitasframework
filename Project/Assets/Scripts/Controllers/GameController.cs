﻿using Entitas;
using UnityEngine;

/// <summary>
/// Main entry point for the program. All systems and dependencies are injected in this class
/// 
/// Adding new services:    1) Create a component for the service in MetaComponents.cs
///                         2) Create the interface for the service
///                         3) Create an implementation of the service interface
///                         4) Add service to the ServiceContainer.cs
///                         5) Create registrationsystem(initialize system) for the service(allows to access service by 
///                             calling: contexts.meta.servicenameComponent.instance
///                         6) Call registrationsystem inside the ServiceRegistrationSystem.cs Feature
///                         7) Add the implementation of the interface in the GameController.cs
/// 
/// 
/// Overview of classes:
/// 
/// Simulation Layer: Contains data and logic given by Entitas. This layer is completely decoupled from the gameEngine(Unity, XNA, Unreal etc.)
///                   This layer communicates with the View using Events(Observer Pattern)
/// Service classes: Store outside source of information(input, audio), 3rd party engines etc.
/// View Layer: Code responsible for displaying the game state to the player(animation, audio, ui) using a specfic game engine
/// 
/// 
/// Event communication:
///             1) Declare the attribute [Event(true)] above the component, whose change should be noted=> 
///                 creates ComponentNameListenerComponent and IComponentNameListener interface
///             2) Create a ComponentNameListener : Monobehaviour, IEvenetListener, IComponentNameListener, which registers itself as
///                    a listener and implements what to do, when a event is fired
///             3) Add ComponentNameListener to any gameobject or prefab in the Unity scene, from which a specific functionality is required
/// Create a ComponentListener: IEventListener, IComponentListener , 
/// </summary>
public class GameController : MonoBehaviour
{
    private Contexts _contexts;

    private Systems _serviceRegistrationSystem;

    // Use this for initialization
    void Start()
    {
        _contexts = Contexts.sharedInstance;
        var serviceContainer = new ServiceContainer(new UnityInputService(),
                                                    new UnityTimeService(),
                                                    new UnityViewService());

        _serviceRegistrationSystem = new ServiceRegistrationSystem(_contexts, serviceContainer);
        _serviceRegistrationSystem.Initialize();
       
    }

    // Update is called once per frame
    void Update()
    {
        //TODO : execute and cleanup systems
    }
}
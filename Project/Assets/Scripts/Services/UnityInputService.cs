﻿using UnityEngine;

public class UnityInputService : IInputService
{
    public float HorizontalInput
    {
        get { return Input.GetAxis("Horizontal"); }
    }

    public bool JumpButtonWasPressed
    {
        get { return Input.GetKeyDown(KeyCode.W); }
    }
}
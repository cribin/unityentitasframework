﻿using Entitas;

/// <summary>
/// This class implements services for loading and displaying various objects to the screen
/// </summary>
public interface IViewService
{
    void LoadAsset(Contexts contexts, IEntity entity, string assetName);
}
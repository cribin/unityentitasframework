﻿


public class ControllerInputService : IInputService
{
    public float HorizontalInput { get; private set; }
    public bool JumpButtonWasPressed { get; private set; }
}


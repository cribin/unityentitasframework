﻿
/// <summary>
/// This class contains all Services used throughout the entire project
/// </summary>
public class ServiceContainer
{
    public readonly IInputService InputService;
    public readonly ITimeService TimeService;
    public readonly IViewService ViewService;

    public ServiceContainer(IInputService inputService, ITimeService timeService, IViewService viewService)
    {
        InputService = inputService;
        TimeService = timeService;
        ViewService = viewService;
    }
}
﻿public interface IInputService
{
    float HorizontalInput { get; }
    bool JumpButtonWasPressed { get; }
}
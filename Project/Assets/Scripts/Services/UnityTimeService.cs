﻿using UnityEngine;

public class UnityTimeService : ITimeService
{
    public float DeltaTime
    {
        get { return Time.deltaTime; }
    }

    public float FixedDeltaTime
    {
        get { return Time.fixedDeltaTime; }
    }
}
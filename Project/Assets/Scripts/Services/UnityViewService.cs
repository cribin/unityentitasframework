﻿using Entitas;
using UnityEngine;

public class UnityViewService : IViewService
{
    public void LoadAsset(Contexts contexts, IEntity entity, string assetName)
    {
        var viewGo = Object.Instantiate(Resources.Load<GameObject>("Prefabs/" + assetName));
        if (viewGo == null) return;
        var viewController = viewGo.GetComponent<IViewController>();
        if (viewController != null) viewController.InitializeView(contexts, entity);

        var eventListeners = viewGo.GetComponents<IEventListener>();
        foreach(var listener in eventListeners)
            listener.RegisterListeners(entity);
    }
}